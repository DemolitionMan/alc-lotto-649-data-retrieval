#!/usr/bin/env sh

# Data Retrieval and Extraction Tool for Atlantic Lottery Corporation's Lotto 649
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Copyright (c) 2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

docker run \
  --rm \
  --interactive --tty \
  --volume ${PWD}/alc:/home/user/project \
  alc ${1}

exit 0

