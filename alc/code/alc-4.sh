#!/usr/bin/env sh

# Lottery Data Retrieval and Extraction for Atlantic Lottery Corporation
# Author: Gregory D. Horne < greg at gregoryhorne dot ca >
# Original source:
#   https://gitlab.com/gregorydhorne/alc-lotto-649-data-retrieval
# Copyright (c) 2017-2018 Gregory D. Horne
# License: BSD 3-Clause License (http://opensource.org/licenses/BSD-3-Clause)

# Configuration parameters.
BROWSER="wget -O ./data/page.html"
PROTOCOL="https"
DOMAIN="www.alc.ca"

# Archive current data after retrieving the latest updates.
archive_data()
{
  local prefetch_count=${1}
  local post_fetch_count=${2}
  local file_name=$(echo ${3} | sed 's/-//g')

  if [[ ${prefetch_count} -lt ${postfetch_count} ]]
  then
    if [[ -e ./data/${file_name}.zip ]]
    then
      zip -f ./data/${file_name}.zip ./data/${file_name}.csv > /dev/null 2>&1
      echo -e "\tArchive updated"
    else
      zip ./data/${file_name}.zip ./data/${file_name}.csv > /dev/null 2>&1
      echo -e "\tArchive created"
    fi
  else if [[ ! -e ./data/${file_name}.zip ]] \
          && [[ ${prefetch_count} -ne 0 ]] \
          && [[ ${postfetch_count} -ne 0 ]]
       then
         zip ./data/${file_name}.zip ./data/${file_name}.csv > /dev/null 2>&1
         echo -e "\tArchive created"
       else
         echo -e "\tNo updates to archive"
       fi
  fi

  return
}

# Extract the JSON formatted data about the lottery from the HTML file.
# Create a well-formed JSON data structure and extract lottery details (gameId
# and gameData).
extract_lottery_details()
{
  local game_designator=${1}

  local file_name=$(echo ${game_designator} | sed 's/-//g')

  local bonus_number
  local jackpot
  local numbers

  awk '
    /ALC.components.GameDetailComponent/ { flag = 1; next; }
    /jQuery/ { flag = 0; } flag
  ' ./data/page.html \
  | awk '
      BEGIN { print("["); }
      /gameId/ {
        sub("gameId", "\"gameId\"", $0);
        printf("{%s", $0);
      }
      /gameData/ {
        sub("gameData", "\"gameData\"", $0);
        sub(/,$/, "},", $0);
        printf("%s\n", $0);
      }
      END { printf("{}\n]"); }
  ' > ./data/page.json

  # Extract the winning numbers and the bonus number.
  numbers=$(jq --raw-output '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.winning_numbers | @csv' ./data/page.json |  sed 's/\"//g')
  bonus_number=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.bonus_number' ./data/page.json | sed 's/\"//g')

  # Extract the jackpot amount.
  jackpot=$(jq '.[] | select(.gameId=="Lotto649") | .gameData | .[].draw.prize_payouts | .[] | select(.type=="Lotto649_6of6") | .prize_value' ./data/page.json)
  jackpot=$(printf "%0.2f" ${jackpot})

  # Delete temporary lottery data files
  rm -f ./data/page.html ./data/page.json

  echo "${numbers},${bonus_number},${jackpot}"
}

# Initialise datastore.
initialise()
{
  local file_name=$(echo ${1} | sed 's/-//g')

  if [[ ! -e ./data ]]
  then
    mkdir ./data
  fi

  if [[ ! -e../data/${file_name}.csv ]]
  then
    touch ./data/${file_name}.csv
  fi

  return
}

# Report current record count.
record_count()
{
  local file_name=$(echo ${1} | sed 's/-//g')

  echo $(wc -l ./data/${file_name}.csv | cut -d \  -f 1)
}

# Retrieve lottery results for the specified draw date.
retrieve_lottery_results()
{
  local game_designator=${1}
  local draw_date=${2}
  local error_count=${3}

  local file_name=$(echo ${game_designator} | sed 's/-//g')

  # If the results for the draw date have already been retrieved, do not fetch
  # them again.
  if [[ ! -e ./data/${file_name}.csv ]] \
     || [[ -z $(grep "${draw_date}" ./data/${file_name}.csv) ]]
  then
    # Launch the web browser and retrieve the Lotto 649 winning numbers for the
    # specified draw date. The date is not validated to ensure it is an actual
    # draw date; if the date is incorrect, the lottery results returned are for
    # the draw date prior to the calendar date.
    ASSET="/content/alc/en/our-games/lotto/${game_designator}.html"
    QUERY="?date=${draw_date}"
    ${BROWSER} ${PROTOCOL}://${DOMAIN}${ASSET}${QUERY} > /dev/null 2>&1
    # Check the web page was saved to a file named page.html. 
    if [[ ! -e ./data/page.html ]]
    then
      error_count=$(expr ${error_count} + 1)
    fi
  fi

  echo ${error_count}
}

status()
{
  local start_count=${1}
  local end_count=${2}
  local error_count=${3}

  echo "Status:"
  echo -e "\tRecord count: ${start_count} (pre-count) : ${end_count} (post-count)"
  echo -e "\tError count: ${error_count}"

  return
}

lotto649()
{
  local start_year=${1}
  local end_year=${2}
  local game_designator=${3}

  local file_name=$(echo ${game_designator} | sed 's/-//g')
  local today=$(date -I)

  local error_count=0

  local days_of_month
  local draw_date
  local line

  for year in $(seq ${start_year} ${end_year})
  do
    for month in $(seq 12)
    do
      cal ${month} ${year} | tail -n +2 | sed 's/   / 0 /g' \
      | awk -v cols=We,Sa '
          BEGIN { split(cols, out, ","); }
          NR == 1 { for (i = 1; i <= NF; i++) { ix[$i] = i; } }
          NR > 1 {
            for (i in out) { printf("%s%s", $ix[out[i]], OFS); }
            print("");
          }' \
      | head -n -1 > ./data/draw-dates

      while IFS='' read -r days_of_week || [[ -n "${days_of_week}" ]]
      do
        for day in $(echo ${days_of_week})
        do
          draw_date=$(printf "%4d-%02d-%02d" ${year} ${month} ${day})
          # Check for a valid day of the month (01-31) inclusive
          if [[ ${draw_date:8:2} -eq 0 ]]; then continue; fi
          historic_date=$(echo ${draw_date} | sed 's/-//g')
          current_date=$(echo ${today} | sed 's/-//g')
          if [[ "${historic_date}" -lt "${current_date}" ]]
          then
            error_count=$(retrieve_lottery_results ${game_designator} ${draw_date} ${error_count})
            if [[ -e ./data/page.html ]]
            then
              results=$(extract_lottery_details ${game_designator})
              write_details_to_file ${file_name}.csv ${draw_date} "${results}"
            fi
          fi
        done
      done < ./data/draw-dates
      rm -f ./data/draw-dates
    done
  done

  temp_file=$(mktemp)
  sort ./data/${file_name}.csv > ${temp_file}
  cp ${temp_file} ./data/${file_name}.csv

  echo ${error_count}
}

# Validate the lottery game designator as well as starting and ending years. 
validate()
{
  local game_designator=${1}
  local start_year=${2}
  local end_year=${3}

  # If a lottery name has not been passed as the first argument, display usage
  # message.
  if [[ "${game_designator}" != "lotto-6-49" ]] \
     || [[ -z ${start_year} ]] \
     || [[ -z ${end_year} ]]
  then
    echo "Usage:    alc.sh lottery"
    echo "Example:  alc.sh lotto-6-49 YYYY YYYY"
    exit 1
  fi
}

# Save the lottery results to a file named lotto649.csv.
write_details_to_file()
{
  local file_name=${1}
  local draw_date=${2}
  local results=${3}

  printf "${draw_date},${results}\n" >> ./data/${file_name}
}

# Retrieve lottery data.
main()
{
  local game_designator=${1}
  local start_year=${2}
  local end_year=${3}

  local prefetch_count=0
  local postfetch_count=0
  local error_count=0

  validate ${game_designator} ${start_year} ${end_year}

  initialise ${game_designator}

  printf "Data retrieval starting..."
  prefetch_count=$(record_count ${game_designator})
  error_count=$(lotto649 ${start_year} ${end_year} ${game_designator})
  printf "completed "
  if [[ "${error_count}" == "0" ]]
  then
    printf "successfully\n"
  else
    printf "with errors\n"
    postfetch_count=$(record_count ${game_designator})
    status ${prefetch_count} ${postfetch_count} ${error_count}
  fi

  archive_data ${prefetch_count} ${postfetch_count} ${game_designator}
}

############################################################################### 

# Pass the lottery game designator, starting year, and ending year to retrieve.
main ${1} ${2} ${3}
exit 0
